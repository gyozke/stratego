module.exports = {
  entry: [
    './src/main.jsx'
  ],
  module: {
    loaders: [{
      test: /\.jsx?$/,
      exclude: /node_modules/,
      loaders: ['babel'],
    }]
  },
  resolve: {
    extensions: ['', '.js', '.jsx']
  },
  output: {
    path: __dirname + '/public/dist/',
    publicPath: '/',
    filename: 'bundle.js'
  },
};
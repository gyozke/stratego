var readableState = {
  players: {
    'red': {
      soldiers: [
        '1234_',
        '6b89g',
        'f____',
        '_____',
        '_____',
      ],
      
      initialNumbers: {
        'flag': 1,
        'bomb': 5,
        'soldier1': 1,
        'soldier2': 8,
        'soldier3': 5,
        'soldier4': 4,
        'soldier5': 4,
        'soldier6': 4,
        'soldier7': 3,
        'soldier8': 2,
        'soldier9': 1,
        'soldier10': 1,
      }
      
    },
    'blue': {
      soldiers: [
        '_____',
        '_____',
        '____b',
        '5679g',
        'f2345',
      ],
      
      initialNumbers: {
        'flag': 1,
        'bomb': 5,
        'soldier1': 1,
        'soldier2': 8,
        'soldier3': 5,
        'soldier4': 4,
        'soldier5': 4,
        'soldier6': 4,
        'soldier7': 3,
        'soldier8': 2,
        'soldier9': 1,
        'soldier10': 1,
      }
      
    },
  },
  battleField: [
    '____X',
    '_____',
    '_XXX_',
    '_____',
    '_____',
  ],
};

var mergedStateExample1x1 = {
  map: [
    [
      {
        isValidField: true,     // true/false
        fieldType: 'grass',     // 'grass', 'rock', 'lake', etc
        isEmpty: false,         // true/false
        player: 'red',          // 'red', 'blue', etc
        type: 'soldier',  // 'soldier', 'flag', 'bomb'
        value: 5,     // 1 .. 10
      }
    ]
  ],
  players: [
    'red',
    'blue'
  ],
  initialNumberOfSoldiers: {
    'red': {
      'flag': 1,
      'bomb': 1,
      'soldier1': 1,
      'soldier10': 1
    },
    'blue': {
      'flag': 1,
      'bomb': 1,
      'soldier1': 1,
      'soldier10': 1
    },
  }
}

function createStateFromReadable(readableState) {
  const map = readableState.battleField.map(row => Array.from(row).map(cell => ({
    isValidField: cell.toUpperCase() !== 'X',
    fieldType:    cell.toUpperCase() === 'X' ? 'lake' : 'grass',
    isEmpty:      true,
  })));
  
  const players = Object.keys(readableState.players);

  // players.forEach(player => {
  //   readableState.players[player].soldiers.forEach((row, i) => {
  //     Array.from(row).forEach((cell, j) => {
  //       map[i][j].isEmpty = (cell === '_');
  //       if (map[i][j].isEmpty)
  //     })
  //   })
  // });
  
  for (let player of players) {
    for (const [i, row] of readableState.players[player].soldiers.entries()) {
      for (const [j, cell] of Array.from(row).entries()) {
        let mapCell = map[i][j]; 
        // console.log(cell, i, j)
        if (/[fbg1-9]/.test(cell)) {
          mapCell.isEmpty = false;
          mapCell.player = player;
          mapCell.type = cell === 'f' ? 'flag' : cell === 'b' ? 'bomb' : 'soldier';
          if (mapCell.type === 'soldier') {
            mapCell.value = cell === 'g' ? 10 : parseInt(cell);
          }
        }
      }
    } 
  }
  
  const initialNumberOfSoldiers = {};
  for (let player of players) {
    initialNumberOfSoldiers[player] = readableState.players[player].initialNumbers;
  }
  
  return {
    map,
    players,
    initialNumberOfSoldiers,
  }
}

export default createStateFromReadable(readableState);


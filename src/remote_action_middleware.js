export default socket => store => next => action => {
  console.log('remote action middleware');
  if (action.meta && action.meta.remote) {
    // console.log(socket);
    if (action.type === 'JOIN_A_GAME') {
      socket.emit('join');
    } else {
      action.gameId = store.getState().gameId;
      socket.emit('action', action);
    }
  }
  if (action.type === 'START_THE_GAME') {
    const state = store.getState();
    socket.emit('sync_state', {
      color: state.player,
      soldiers: state.soldiers.filter(s => s.color === state.player),
      gameId: state.gameId,
    });
  }
  return next(action);
}


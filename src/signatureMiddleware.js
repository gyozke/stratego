export default store => next => action => {
  console.log('signature middleware');
  if (!(action.meta && action.meta.sender)) {
    action.meta = action.meta || {};
    action.meta.sender = store.getState().player;
    action.meta.playerToTurn = action.meta.playerToTurn || store.getState().playerToTurn;
  }
  return next(action);
}
import React from 'react';
import ReactDOM from 'react-dom';
// import Main from './components/Main';
import POCApp from './components/POCApp';
import state from './data';

console.log('start')

ReactDOM.render(
  <POCApp />,
  document.getElementById('container')
);

/*
ReactDOM.render(
  <Main map={state.map} />,
  document.getElementById('container')
);
*/
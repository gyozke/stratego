export default store => next => action => {
  console.log('authorization middleware');
  if (action.meta && action.meta.needAuthorization) {
    if (action.meta.sender !== action.meta.playerToTurn) {
      console.log('Authorization failed', action.meta.sender, action.meta.playerToTurn);
      return;
    }
  }
  return next(action);
}
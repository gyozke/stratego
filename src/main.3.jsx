import React from 'react';
import ReactDOM from 'react-dom';
// import Main from './components/Main';
import POCApp from './components/POCApp';

import { compose, createStore } from 'redux';
import { Provider } from 'react-redux';
import reducer from './redux/';

let store = createStore(reducer,
  window.devToolsExtension ? window.devToolsExtension() : undefined
);

ReactDOM.render(
  <Provider store={store}>
    <POCApp />
  </Provider>,
  document.getElementById('container')
);

// Log the initial state
// console.log(store.getState())

// Every time the state changes, log it
// let unsubscribe = store.subscribe(() =>
//   console.log(store.getState())
// )

// Dispatch some actions
// store.dispatch(selectSoldierAction(2, 3))

// Stop listening to state updates
// unsubscribe()
import React from 'react';
import ReactDOM from 'react-dom';

import POCApp from './components/POCApp';
import POCGame from './components/POCGame';

import { createStore, combineReducers, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import reducer from './redux/';
import { Router, Route, browserHistory, hashHistory } from 'react-router';
import { syncHistoryWithStore, routerReducer } from 'react-router-redux'; 

import io from 'socket.io-client';

import remoteActionMiddleware from './remote_action_middleware';
import signatureMiddleware from './signatureMiddleware';
import authorizationMiddleware from './authorizationMiddleware';

import { joinedToTheGame, startTheGame, syncState } from './redux/actions';
// A react-router-redux-hoz
// const combinedReducers = combineReducers({
//   gameState: reducer,
//   routing: routerReducer,
// });

const socket = io(`${location.protocol}//${location.hostname}:${location.port}`);

socket.on('joined', function (data) {
  store.dispatch(joinedToTheGame(data));
});
socket.on('startGame', function () {
  store.dispatch(startTheGame());
});
socket.on('action', function (action) {
  console.log('server action', action);
  if (action.meta) {
    delete action.meta.remote;
  }
  store.dispatch(action);
});
socket.on('sync_state', function (data) {
  store.dispatch(syncState(data));
})

const createStoreWithMiddleware = applyMiddleware(
  signatureMiddleware,
  authorizationMiddleware,
  remoteActionMiddleware(socket)
)(createStore);

const store = createStoreWithMiddleware(reducer,
  window.devToolsExtension ? window.devToolsExtension() : undefined
);

// const history = syncHistoryWithStore(hashHistory, store);



ReactDOM.render(
  <Provider store={store}>
    <POCApp />
  </Provider>,
  document.getElementById('container')
);

/*
ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <Route path="/" component={POCApp}>
        <Route path="game" component={POCGame}/>
      </Route>
    </Router>
  </Provider>,
  document.getElementById('container')
);
*/

// Log the initial state
// console.log(store.getState())

// Every time the state changes, log it
// let unsubscribe = store.subscribe(() =>
//   console.log(store.getState())
// )

// Dispatch some actions
// store.dispatch(selectSoldierAction(2, 3))

// Stop listening to state updates
// unsubscribe()
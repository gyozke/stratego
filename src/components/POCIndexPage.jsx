import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actionCreators from '../redux/actions';
import POCAnonymousBlokk from './POCAnonymousBlokk';

class POCIndexPage extends React.Component {
  render() {
    return (
      <div>
        <h2>Main Page</h2>
        <POCAnonymousBlokk actions={this.props.actions} />
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return { actions: bindActionCreators(actionCreators, dispatch) }
}

function mapStateToProps(state) {
  return state;
}

export default connect(mapStateToProps, mapDispatchToProps)(POCIndexPage);
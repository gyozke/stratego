import React from 'react';
import Panel from './Panel';
import ListGroup from './ListGroup';
import Map from './Map';

class App extends React.Component {
  render() {
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-xs-3">
            <Panel>
              <ListGroup />
            </Panel>
          </div>
          <div className="col-xs-6">
            <Panel>
              <Map map={this.props.map} />
            </Panel>
          </div>
          <div className="col-xs-3">
            <Panel>
              <ListGroup />
            </Panel>
          </div>
        </div>
      </div>
      );
  }
}

export default App;
import React from 'react';
import { Link } from 'react-router';

class POCApp extends React.Component {
  render() {
    console.log(this.props)
    
    return (
      <div>
        <h1>Stratego</h1>
        <h2>Menu</h2>
        <ul>
          <li><Link to="/">Home</Link></li>
          <li><Link to="/game">Game</Link></li>
        </ul>
        
        <div>
          {this.props.children}
        </div>
        
      </div>
    );
  }
}

export default POCApp;
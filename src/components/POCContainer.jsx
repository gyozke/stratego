import React from 'react';
import POCSquare from './POCSquare';
import POCField from './POCField';

class POCContainer extends React.Component {
  render() {
    
    const n = this.props.height;
    const m = this.props.width;
    
    const fields = [];
    for (let i = 0; i < n; i++) {
      for (let j = 0; j < m; j++) {
        fields.push(
          <POCField 
            x={j} 
            y={i} 
            key={`f_${j}_${i}`} 
            actions={this.props.actions}
            highlighted={this.props.validFields && this.props.validFields.some(f => f.x === j && f.y === i)}
          />
        );
      }
    }
    
    const soldiers = this.props.soldiers.map(soldier =>
      <POCSquare 
        {...soldier} 
        key={soldier.id} 
        actions={this.props.actions}
        selected={this.props.selected ? this.props.selected.id === soldier.id : false}
        value={soldier.color === this.props.player ? soldier.value : null}
      />
    )
    
    return (
      <div id="POCtable">
        
        {fields}
      
        {soldiers}
        
      </div>
    );
  }
}

export default POCContainer;
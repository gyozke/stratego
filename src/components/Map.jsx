import React from 'react';
import Square from './Square';

class Map extends React.Component {
  render() {
    const numberOfRows = this.props.map.length;
    const numberOfColums = this.props.map[0].length;
    const size = 100 / numberOfColums;
    
    const cells = this.props.map.reduce((row, prev) => row.concat(prev), []);
    const squares = cells.map((cell, i) => (
      <Square 
          size={`${size}%`} 
          {...cell}
          key={i}
      />
    ));
    // this.props.map.forEach((row, i) => {
    //   row.forEach((cell, j) => {
    //     squares.push();
    //   })  
    // })
    
    return (
      <div className="map cf">
        {squares}
      </div>
      );
  }
}

export default Map;
import React from 'react';

class POCAnonymousBlokk extends React.Component {
  render() {
    return (
      <div>
        <h3>Public room</h3>
        <input 
          type="button" 
          value="Start Game" 
          className="btn btn-default"
          onClick={() => this.props.actions.startPublicGame()}
        />
      </div>
    );
  }
}

export default POCAnonymousBlokk;
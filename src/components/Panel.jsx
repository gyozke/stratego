import React from 'react';
import Map from './Map';

class Panel extends React.Component {
  render() {
    return (
      <div className="panel panel-default">
        { /* Default panel contents */ }
        <div className="panel-heading">Panel heading</div>
        <div className="panel-body">
          <p>...</p>
        </div>
        
        {this.props.children}
        
      </div>
      );
  }
}

export default Panel;
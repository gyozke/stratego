import React from 'react';

class Entity extends React.Component {
  render() {
    return (
      <div className={`entity ${this.props.player} ${this.props.type} text-center`}>
        {this.props.value}
      </div>
    );
  }
}

export default Entity;
import React from 'react';
import POCIndexPage from './POCIndexPage';
import POCGame from './POCGame';
import { connect } from 'react-redux';

class POCApp extends React.Component {
  render() {
    // console.log(this.props)
    
    const mapPageToComponent = {
      'index': POCIndexPage,
      'game': POCGame,
    };
    
    const Page = mapPageToComponent[this.props.application.page];
    
    return (
      <div>
        <h1>Stratego</h1>
        
        <div>
          <Page />  
        </div>
        
      </div>
    );
  }
}

export default connect(state => state)(POCApp);
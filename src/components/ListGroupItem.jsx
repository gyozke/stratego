import React from 'react';

class ListGroupItem extends React.Component {
  render() {
    return (
      <li className="list-group-item">
        Soldier 10
        <span className="badge">3/14</span>
      </li>
      );
  }
}

export default ListGroupItem;
import React from 'react';

class POCBattle extends React.Component {
  componentDidMount() {
    const playerToTurn = this.props.playerToTurn;
    console.log('POCBattle', playerToTurn);
    setTimeout(() => {
      this.props.actions.endBattle(playerToTurn);
    }, 1000);
  }
  render() {
    return (
      <div 
        className="POCbattle" 
      >
        <h2>Harc</h2>
        <p>{this.props.color1}: {this.props.value1}</p>
        <p>{this.props.color2}: {this.props.value2}</p>
      </div>
    );
  }
}

export default POCBattle;
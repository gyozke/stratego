import React from 'react';
import ReactDOM from 'react-dom';

class POCSquare extends React.Component {
  componentDidMount() {
    const el = ReactDOM.findDOMNode(this);
    el.addEventListener('transitionend', e => {
      console.log(e)
      this.props.actions.endMoving(this.props.id);
    }, false);
  }
  render() {
    const style = {
      transform: `translate3d(${this.props.x * 40 + 10}px, ${this.props.y * 40 + 5}px, 0px)`,
      backgroundColor: this.props.color,
      borderColor: this.props.selected ? 'orange' : 'white',
    }
    
    return (
      <div 
        className="POCsquare" 
        style={style}
        onClick={() => this.props.actions.selectSoldier(this.props.id, this.props.x, this.props.y)}
      >
        {this.props.value}
      </div>
    );
  }
}

export default POCSquare;
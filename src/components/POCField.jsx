import React from 'react';

class POCField extends React.Component {
  render() {
    const style = {
      transform: `translate3d(${this.props.x * 40}px, ${this.props.y * 40}px, 0px)`,
      backgroundColor: this.props.highlighted ? 'orange' : '',
    }
    return (
      <div 
        className="POCfield" 
        style={style}
        onClick={() => this.props.actions.selectField(this.props.x, this.props.y)}
      ></div>
    );
  }
}

export default POCField;
import React from 'react';
import App from './App';

class Main extends React.Component {
  render() {
    return (
      <App map={this.props.map} />
    );
  }
}

export default Main;
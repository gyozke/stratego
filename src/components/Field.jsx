import React from 'react';

class Field extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className={`field ${this.props.fieldType}`} />
    );
  }
}
Field.defaultProps = {
  fieldType: 'grass'
};

export default Field;
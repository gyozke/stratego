import React from 'react';
import Field from './Field';
import Entity from './Entity';

class Square extends React.Component {
  render() {
    return (
      <div className="square" style={{
        width: this.props.size,
        paddingTop: this.props.size
      }}>
        <Field fieldType={this.props.fieldType} />
        {!this.props.isEmpty 
          ? <Entity {...this.props}></Entity> 
          : null}
      </div>
      );
  }
}

export default Square;
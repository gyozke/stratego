import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as actionCreators from '../redux/actions';
import { getValidFields } from '../redux/';

import POCContainer from './POCContainer';
import POCBattle from './POCBattle';
import POCWin from './POCWin';

class POCGame extends React.Component {
  componentDidMount() {
    this.props.actions.joinAGame();
  }
  render() {
    console.log(this.props)
    return (
      <div>
        <p>Your color: {this.props.player}</p>
        <p>State: {this.props.gameState} ({this.props.playerToTurn})</p>
        <p>{this.props.player === this.props.playerToTurn ? 'You turn!' : 'Wait!'}</p>
        <POCContainer
          {...this.props}
          height={this.props.dimensions.height}
          width={this.props.dimensions.width}
          soldiers={this.props.soldiers}
          actions={this.props.actions}
          selected={this.props.selected}
          validFields={this.props.validFields}
        />
        {this.props.gameState === 'BATTLE' 
          ? <POCBattle 
              color1={this.props.selected.color} 
              value1={this.props.selected.value}
              color2={this.props.attacked.color} 
              value2={this.props.attacked.value}
              actions={this.props.actions}
              playerToTurn={this.props.playerToTurn} 
            /> 
          : null}
        {this.props.gameState === 'WIN' 
          ? <POCWin 
              color={this.props.selected.color} 
            /> 
          : null}
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return { actions: bindActionCreators(actionCreators, dispatch) }
}

function mapStateToProps(state) {
  // state = state.gameState;
  return {
    ...state,
    validFields: state.selected && state.player === state.playerToTurn 
      ? getValidFields(state, state.selected.x, state.selected.y)
      : false
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(POCGame);
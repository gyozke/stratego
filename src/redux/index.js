import { routerReducer } from 'react-router-redux'; 

const initialSoldiers = {
  red: [
    { id: 'red_1_1', x: 0, y: 0, color: 'red', type: 'soldier', value: 1 },
    { id: 'red_2_1', x: 1, y: 0, color: 'red', type: 'soldier', value: 2 },
    { id: 'red_3_1', x: 2, y: 0, color: 'red', type: 'soldier', value: 3 },
    { id: 'red_4_1', x: 3, y: 0, color: 'red', type: 'soldier', value: 4 },
    { id: 'red_5_1', x: 4, y: 0, color: 'red', type: 'soldier', value: 5 },
    { id: 'red_6_1', x: 5, y: 0, color: 'red', type: 'soldier', value: 6 },
    { id: 'red_7_1', x: 0, y: 1, color: 'red', type: 'soldier', value: 7 },
    { id: 'red_8_1', x: 1, y: 1, color: 'red', type: 'soldier', value: 8 },
    { id: 'red_9_1', x: 2, y: 1, color: 'red', type: 'soldier', value: 9 },
    { id: 'red_10_1', x: 3, y: 1, color: 'red', type: 'soldier', value: 10 },
    { id: 'red_b_1', x: 4, y: 1, color: 'red', type: 'bomb', value: 'B' },
    { id: 'red_f_1', x: 5, y: 1, color: 'red', type: 'flag', value: 'F' },
  ],
  blue: [
    { id: 'blue_1_1', x: 0, y: 4, color: 'blue', type: 'soldier', value: 1 },
    { id: 'blue_2_1', x: 1, y: 4, color: 'blue', type: 'soldier', value: 2 },
    { id: 'blue_3_1', x: 2, y: 4, color: 'blue', type: 'soldier', value: 3 },
    { id: 'blue_4_1', x: 3, y: 4, color: 'blue', type: 'soldier', value: 4 },
    { id: 'blue_5_1', x: 4, y: 4, color: 'blue', type: 'soldier', value: 5 },
    { id: 'blue_6_1', x: 5, y: 4, color: 'blue', type: 'soldier', value: 6 },
    { id: 'blue_7_1', x: 0, y: 5, color: 'blue', type: 'soldier', value: 7 },
    { id: 'blue_8_1', x: 1, y: 5, color: 'blue', type: 'soldier', value: 8 },
    { id: 'blue_9_1', x: 2, y: 5, color: 'blue', type: 'soldier', value: 9 },
    { id: 'blue_10_1', x: 3, y: 5, color: 'blue', type: 'soldier', value: 10 },
    { id: 'blue_b_1', x: 4, y: 5, color: 'blue', type: 'bomb', value: 'B' },
    { id: 'blue_f_1', x: 5, y: 5, color: 'blue', type: 'flag', value: 'F' },
  ],
};

const initialState = {
  routing: {
    locationBeforeTransitions: null
  },
  application: {
    page: 'index',
  },
  player: 'red',
  playerToTurn: 'red',
  gameState: 'SELECTING',
  dimensions: {
    width: 6,
    height: 6,
  },
  soldiers: []
};

export default function reducer(state = initialState, action) {
  console.log(action)
	switch (action.type) {
		case 'SELECT_SOLDIER':
      return selectSoldier(state, action.payload);
    case 'SELECT_FIELD':
      return selectField(state, action.payload);
    case 'END_MOVING':
      return endMoving(state, action.payload);
    case 'END_BATTLE':
      return endBattle(state);
    case 'START_PUBLIC_GAME':
      return startPublicGame(state);
    case 'JOIN_A_GAME':
      return joinAGame(state);
    case 'JOINED_TO_THE_GAME':
      return joinedToTheGame(state, action.payload);
    case 'START_THE_GAME':
      return startTheGame(state);
    case 'SYNC_STATE':
      return syncState(state, action.payload);
    case '@@router/LOCATION_CHANGE':
      const newState = copy(state);
      newState.routing = routerReducer(state.routing, action);
      return newState;
	}
	return state;
}

function findSoldierById(state, id) {
  for (let i = 0; i<state.soldiers.length; i++) {
    let s = state.soldiers[i];
    if (s.id === id) {
      return {
        soldier: s,
        index: i,
      };
    }
  }
}

function copy(state) {
  return JSON.parse(JSON.stringify(state));
}

function shuffle(array) {
  var m = array.length, i;

  // While there remain elements to shuffle…
  while (m) {

    // Pick a remaining element…
    i = Math.floor(Math.random() * m--);

    // And swap it with the current element.
    const {x, y} = array[m];
    array[m].x = array[i].x;
    array[m].y = array[i].y;
    array[i].x = x;
    array[i].y = y;
  }

  return array;
}

function getSoldierByXY(state, x, y) {
  for (let i = 0; i<state.soldiers.length; i++) {
    let s = state.soldiers[i];
    if (s.x === x && s.y === y) {
      return {
        soldier: s,
        index: i,
      };
    }
  }
  return false;
}

export function getValidFields(state, x, y) {
  function goodField(row, col, origRow, origCol, distance) {
    
    function allClearBetween() {
      if (row - origRow !== 0) {
        let startY = Math.min(row, origRow);
        let endY = Math.max(row, origRow);
        let x = col;
        
        startY += 1;
        while (startY < endY && !getSoldierByXY(state, x, startY)) {
          startY += 1;
        }
        return startY === endY;
      }
      else {
        let startX = Math.min(col, origCol);
        let endX = Math.max(col, origCol);
        let y = row;
        
        // console.log('eleje', startX, endX, y);
        
        startX += 1;
        while (startX < endX && !getSoldierByXY(state, startX, y)) {
          startX += 1;
        }
        
        // console.log('vege', startX, endX, y);
        
        return startX === endX;
      }
    }
    
    let good = true;
    let maxRow = state.dimensions.height;
    let maxCol = state.dimensions.width;
    
    // console.log(row, col);
    good = good && row >= 0 && row <= maxRow-1 && col >= 0 && col <= maxCol-1;
    // console.log(1, good);
    good = good && (row - origRow === 0 || col - origCol === 0);
    // console.log(2, good);
    good = good && !(row === origRow && col === origCol);
    // console.log(3, good);
    good = good && Math.max(Math.abs(row-origRow), Math.abs(col-origCol)) <= distance;
    // console.log(4, good);
    good = good && allClearBetween();
    // console.log(5, good);
    
    let soldierRC = getSoldierByXY(state, col, row);
    let soldierOrigRC = getSoldierByXY(state, origCol, origRow);
    
    // console.log(soldierRC, soldierOrigRC);
    if (soldierRC && soldierOrigRC) {
      good = good && soldierRC.soldier.color !== soldierOrigRC.soldier.color;
    }
    // console.log(6, good);

    return good;
  }

  const distance = {
    1: 1,
    2: Infinity,
    3: 1,
    4: 1,
    5: 1,
    6: 1,
    7: 1,
    8: 1,
    9: 1,
    10: 1,
    'B': 0,
    'F': 0,
  };
  
  const validFields = [];
  
  const soldier = getSoldierByXY(state, x, y);
  
  if (soldier) {
  
    for (let i = 0; i < state.dimensions.height; i++) {
      for (let j = 0; j < state.dimensions.width; j++) {
        if (goodField(i, j, y, x, distance[soldier.soldier.value])) {
          validFields.push({x: j, y: i});
        }
      }
    }
  
  }
  
  return validFields;
}

function selectSoldier(state, {id, x, y}) {
  
  // console.log(JSON.stringify(getValidFields(state, x, y)));
  
  // if (state.player !== state.playerToTurn) {
  //   return state;
  // }
  
  if (state.gameState === 'SELECTING') {
    
    const {soldier: selected} = findSoldierById(state, id);
    
    if (selected.color !== state.playerToTurn) {
      return state;
    }
    
    return Object.assign(copy(state), {
      selected: findSoldierById(state, id).soldier, //{id, x, y},
      gameState: 'SELECTED'
    });
  }
  else if (state.gameState === 'SELECTED') {
    
    const {soldier: selected} = findSoldierById(state, state.selected.id);
    const {soldier: act} = findSoldierById(state, id);
    
    if (selected.color === act.color) {
      return Object.assign(copy(state), {
        selected: findSoldierById(state, id).soldier, // {id, x, y},
        gameState: 'SELECTED'
      });
    }
    else {
      
      if (!getValidFields(state, state.selected.x, state.selected.y)
            .some(pos => pos.x === x && pos.y === y)) {
        return state;
      }
      
      const newState = copy(state);
      for (const s of newState.soldiers) {
        if (s.id === state.selected.id) {
          s.x = x;
          s.y = y;
          break;
        }
      }
      
      return Object.assign(newState, {
        gameState: 'MOVING',
        attacked: findSoldierById(state, id).soldier, // {id, x, y}
      });
    }
  }
  return state;
}

function selectField(state, {x, y}) {
  
  if (state.gameState !== 'SELECTED') {
    return state;
  }
  
  if (!getValidFields(state, state.selected.x, state.selected.y)
        .some(pos => pos.x === x && pos.y === y)) {
    return state;
  }
  
  if (state.soldiers.some(soldier => soldier.x === x && soldier.y === y)) {
    return state;
  }
  
  const newState = copy(state);
  
  for (const s of newState.soldiers) {
    if (s.id === state.selected.id) {
      s.x = x;
      s.y = y;
      break;
    }
  }
  
  return Object.assign(newState, {
    gameState: 'MOVING'
  });
}

function endMoving(state, id) {
  if (state.gameState !== 'MOVING') {
    return state;
  }
  
  if (!state.attacked) {
    return Object.assign(copy(state), {
      gameState: 'SELECTING',
      selected: false,
      playerToTurn: state.playerToTurn === 'red' ? 'blue' : 'red',
    });
  }
  else {
    return Object.assign(copy(state), {
      gameState: 'BATTLE',
    });
  }  
}

function endBattle(state) {
  if (state.gameState !== 'BATTLE') {
    return state;
  }
  
  // console.log(state);
  const {soldier: selected, index: i} = findSoldierById(state, state.selected.id);  
  const {soldier: attacked, index: j} = findSoldierById(state, state.attacked.id);  
  
  const newState = copy(state);
  
  newState.gameState = 'SELECTING';
  
  if (attacked.type === 'bomb') {
    if (selected.value === 3) {
      newState.soldiers.splice(j, 1);  
    } else {
      newState.soldiers.splice(i, 1);
    }
  }
  else if (attacked.type === 'flag') {
    // Nyer
    newState.gameState = 'WIN';
    // newState.winner = selected.color;
  }
  else {
    if (selected.value === 1 && attacked.value === 10) {
      newState.soldiers.splice(j, 1);
    }
    else if (selected.value > attacked.value) {
      newState.soldiers.splice(j, 1);
    } 
    else if (selected.value < attacked.value) {
      newState.soldiers.splice(i, 1);
    }
    else {
      newState.soldiers.splice(Math.max(i, j), 1);
      newState.soldiers.splice(Math.min(i, j), 1);
    }  
  }
  
  if (newState.gameState === 'SELECTING') {
    return Object.assign(newState, {
      attacked: false,
      selected: false,
      playerToTurn: state.playerToTurn === 'red' ? 'blue' : 'red',
    });
  } else {
    return newState;
  }
}

function startPublicGame(state) {
  const newState = copy(state);
  newState.application.page = "game";
  
  return newState;
}

function joinAGame(state) {
  const newState = copy(state);
  newState.gameState = "JOINING";
  return newState;
}

function joinedToTheGame(state, data) {
  const newState = copy(state);
  newState.gameState = "JOINED";
  newState.player = data.color;
  newState.playerId = data.playerId;
  newState.gameId = data.gameId;
  
  const shuffledSoldiers = shuffle(initialSoldiers[newState.player]);
  newState.soldiers = newState.soldiers.concat(shuffledSoldiers);
  
  return newState;
}

function startTheGame(state, data) {
  const newState = copy(state);
  newState.gameState = "SELECTING";
  return newState;
}

function syncState(state, data) {
  const newState = copy(state);

  newState.soldiers = newState.soldiers.concat(data.soldiers);
  
  return newState;
}
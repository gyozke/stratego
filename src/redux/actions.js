export function selectSoldier(id, x, y) {
  return {
    type: 'SELECT_SOLDIER',
    payload: {id, x, y},
    meta: {
      remote: true,
      needAuthorization: true,
    }
  };
};

export function selectField(x, y) {
  return {
    type: 'SELECT_FIELD',
    payload: {x, y},
    meta: {
      remote: true,
      needAuthorization: true,
    }
  };
};

export function endMoving(id) {
  return {
    type: 'END_MOVING',
    payload: id,
    meta: {
      remote: true,
      needAuthorization: true,
    }
  }
}

export function endBattle(playerToTurn) {
  console.log('action endBattle', playerToTurn);
  return {
    type: 'END_BATTLE',
    meta: {
      remote: true,
      needAuthorization: true,
      playerToTurn
    }
  }
}

export function startPublicGame() {
  return {
    type: 'START_PUBLIC_GAME',
  }
}

export function joinAGame() {
  return {
    type: 'JOIN_A_GAME',
    meta: {
      remote: true,
    }
  }
}

export function joinedToTheGame(serverData) {
  return {
    type: 'JOINED_TO_THE_GAME',
    payload: serverData,
  }
}

export function startTheGame() {
  return {
    type: 'START_THE_GAME',
  }
}

export function syncState(serverData) {
  return {
    type: 'SYNC_STATE',
    payload: serverData,
  }
}
/*

// socket's rooms
socket.rooms

// sending to sender-client only
socket.emit('message', "this is a test");

// sending to all clients, include sender
io.emit('message', "this is a test");

// sending to all clients except sender
socket.broadcast.emit('message', "this is a test");

// sending to all clients in 'game' room(channel) except sender
socket.broadcast.to('game').emit('message', 'nice game');

// sending to all clients in 'game' room(channel), include sender
io.in('game').emit('message', 'cool game');

// sending to sender client, only if they are in 'game' room(channel)
socket.to('game').emit('message', 'enjoy the game');

// sending to all clients in namespace 'myNamespace', include sender
io.of('myNamespace').emit('message', 'gg');

// sending to individual socketid
socket.broadcast.to(socketid).emit('message', 'for your eyes only');

*/

// Module dependencies
var express = require('express');
var http = require('http');
var socketio = require('socket.io');

// Instantiating the main app/io combo
var app = express();
var server = http.createServer(app);
var io = socketio.listen(server);

// Middlewares
app.use(express.static('public'));

// Basic room management
function generateRandomString() {
  return Date.now().toString() + Math.floor(Math.random() * 1000000);
}

var maxNumberOfPlayersPerGame = 2;
var numberOfPlayersInActGame = 0;
var actGame = generateRandomString();

io.on('connection', function (socket) {
  console.log('connection', socket.id);
  socket.on('join', function () {
    console.log('join', socket.id, actGame);
    
    var gameId = actGame;
    var playerId = generateRandomString();
    socket.join(gameId);
    socket.emit('joined', {
      gameId,
      playerId,
      color: numberOfPlayersInActGame % 2 === 0 ? 'red' : 'blue',
    });
    // io.to(gameId).emit('joined', {
    //   gameId,
    //   playerId,
    // });
    
    numberOfPlayersInActGame++;
    if (numberOfPlayersInActGame === maxNumberOfPlayersPerGame) {
      io.to(gameId).emit('startGame');
      // console.log(socket.rooms);      
      actGame = generateRandomString();
      numberOfPlayersInActGame = 0;
    }
  });
  socket.on('action', function (action) {
    // console.log('action', action);
    // const room = Object.keys(socket.rooms).shift();
    const room = action.gameId;
    socket.broadcast.to(room).emit('action', action);
  });
  socket.on('sync_state', function (data) {
    console.log(data);
    socket.broadcast.to(data.gameId).emit('sync_state', data);
  });
});

// Start listening
var port = process.env.PORT || 3000;
server.listen(port, "0.0.0.0", function(){
    console.log("Express server listening on port " + port);
    console.log('Visit http://localhost:%s to start play the game', port);
});